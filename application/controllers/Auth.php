<?php
class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('language');
        $this->load->helper('url');
        $lang = $this->session->userdata('language');
        $this->lang->load('view_lang', $lang);
    }

    public function index()
    {
        if ($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->view('forum/header', $data);
            $this->load->view('forum/auth_success', $data);
            $this->load->view('forum/footer');
        }
        else {
            $this->load->helper(array('form'));
            $this->load->view('forum/header_reg');
            $this->load->view('forum/auth_form');
            $this->load->view('forum/footer');
        }
    }
}