<?php
class Reg extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->library('session');
        $this->load->helper('language');
        $lang = $this->session->userdata('language');
        $this->lang->load('view', $lang);
        $this->config->set_item('language', $lang);
    }
    public function index() {
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->view('forum/header_reg');
        $this->load->database();
        $this->load->helper(array('form','url'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'required|callback_username_check|is_unique[users.login]');
        $this->form_validation->set_rules('password', 'Password', 'required|callback_pass_check');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('forum/regform');
            $this->load->view('forum/footer');
        }
        else
        {
            $this->login = $_POST['username'];
            $this->password = md5($_POST['password']);
            $this->email = $_POST['email'];
            $this->registered_at = date("Y-m-d H:i:s");
            $this->img = 0;
            $this->db->insert('users',$this);
            $this->load->view('forum/formsuccess');

        }
    }
    public function username_check($str)
    {
        if ($str == 'admin') {
            $this->form_validation->set_message('username_check', 'The {field} field can not be the word "admin" ');
            return FALSE;
        } else {
            return TRUE;
        }
    }
    public function pass_check() {
        if ($_POST['password'] != $_POST['passconf']) {
            $this->form_validation->set_message('pass_check', 'Password and Password confirmation do not match!');
            return FALSE;
        }
        else {
            return TRUE;
        }
        }

}