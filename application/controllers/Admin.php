<?php

class Admin extends CI_Controller

{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('forum_model');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('language');
        $lang = $this->session->userdata('language');
        $this->lang->load('view_lang', $lang);
        if (!$this->session->userdata('logged_in')) {
            $sess_array = array(
                'id' => 0,
                'username' => 'guest',
                'level' => 0
            );
            $this->session->set_userdata('guest', $sess_array);
        }
    }

    public function index()
    {
        if ($this->session->userdata('logged_in')) {
            $level = $_SESSION['logged_in']['level'];
            if ($level === "1") {
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $this->load->view('forum/header', $data);
                $this->load->view('forum/admin', $data);
                $this->load->view('forum/footer');
            }
            else {
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $this->load->view('forum/header', $data);
                $this->load->view('forum/admin_fail', $data);
                $this->load->view('forum/footer');
            }
        }
        else {
            $session_data = $this->session->userdata('guest');
            $data['username'] = $session_data['username'];
            $this->load->view('forum/header_guest', $data);
            $this->load->view('forum/admin_fail', $data);
            $this->load->view('forum/footer');
        }

    }

    public function manage_content($id=NULL)
    {
        if ($this->session->userdata('logged_in')) {
            $level = $_SESSION['logged_in']['level'];
            if ($level === "1")
            {
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $data['id'] = $id;
                $data['categories'] = $this->forum_model->get_admin_content();
                $data['threads'] = $this->forum_model->get_admin_content($id);
                $data['current_category'] = $this->forum_model->get_current_category($id);
                $this->load->view('forum/header', $data);
                $this->load->view('forum/admin_manage', $data);
                $this->load->view('forum/footer');
            }
            else {
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $this->load->view('forum/header', $data);
                $this->load->view('forum/admin_fail', $data);
                $this->load->view('forum/footer');
            }
        }
        else {
            redirect('home');
        }

    }

    public function delete_category($id)
    {
        if ($session_data = $this->session->userdata('logged_in')) {
            $level = $_SESSION['logged_in']['level'];
            if ($level === "1") {
                $this->forum_model->delete_category($id);
                redirect('admin/manage_content');
            } else {
                redirect('home');
            }
        }
        else {
            redirect('home');
        }
    }

    public function delete_thread($id)
    {
        if ($session_data = $this->session->userdata('logged_in')) {
            $level = $_SESSION['logged_in']['level'];
            if ($level === "1") {
                $this->forum_model->delete_thread($id);
                redirect('admin/manage_content');
            } else {
                redirect('home');
            }
        } else {
            redirect('home');
        }
    }

    public function edit_category($id)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        if ($session_data = $this->session->userdata('logged_in')) {
            $level = $_SESSION['logged_in']['level'];
            if ($level === "1") {
                if ($this->form_validation->run() === FALSE) {
                    $data['category'] = $this->forum_model->get_current_category($id);
                    $session_data = $this->session->userdata('logged_in');
                    $data['username'] = $session_data['username'];
                    $this->load->view('forum/header', $data);
                    $this->load->view('forum/edit_category', $data);
                    $this->load->view('forum/footer');
                } else {
                    $this->forum_model->edit_category($id);
                    redirect('admin/manage_content', 'refresh');
                }
            } else {
                redirect('home');
            }
        }
        else {
            redirect('home');
        }
    }

    public function edit_thread($id)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Subject', 'required');
        if ($session_data = $this->session->userdata('logged_in')) {
            $level = $_SESSION['logged_in']['level'];
            if ($level === "1") {
                if ($this->form_validation->run() === FALSE) {
                    $data['thread'] = $this->forum_model->get_current_thread($id);
                    $data['categories'] = $this->forum_model->get_categories();
                    $session_data = $this->session->userdata('logged_in');
                    $data['username'] = $session_data['username'];
                    $this->load->view('forum/header', $data);
                    $this->load->view('forum/edit_thread', $data);
                    $this->load->view('forum/footer');
                } else {
                    $this->forum_model->edit_thread($id);
                    redirect('admin/manage_content', 'refresh');
                }
            } else {
                redirect('home');
            }
        }
        else {
            redirect('home');
        }
    }

    public function edit_users()
    {
        if ($this->session->userdata('logged_in')) {
            $level = $_SESSION['logged_in']['level'];
            if ($level === "1") {
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $data['users'] = $this->forum_model->get_users();
                $this->load->view('forum/header', $data);
                $this->load->view('forum/admin_users', $data);
                $this->load->view('forum/footer');
            }
            else {
                redirect('home');
            }
        }
        else {
            redirect('home');
        }

    }

    public function make_admin($id)
    {
        if ($this->session->userdata('logged_in')) {
            $level = $_SESSION['logged_in']['level'];
            if ($level === "1") {
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $data['users'] = $this->forum_model->make_admin($id);
                redirect('admin/edit_users');
            }
            else {
                redirect('home');
            }
        }
        else {
            redirect('home');
        }

    }
    public function make_user($id)
    {
        if ($this->session->userdata('logged_in')) {
            $level = $_SESSION['logged_in']['level'];
            if ($level === "1") {
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $data['users'] = $this->forum_model->make_user($id);
                redirect('admin/edit_users');
            }
            else {
                redirect('home');
            }
        }
        else {
            redirect('home');
        }

    }
    public function delete_user($id)
    {
        if ($this->session->userdata('logged_in')) {
            $level = $_SESSION['logged_in']['level'];
            if ($level === "1") {
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $data['users'] = $this->forum_model->delete_user($id);
                redirect('admin/edit_users');
            }
            else {
                redirect('home');
            }
        }
        else {
            redirect('home');
        }

    }

}