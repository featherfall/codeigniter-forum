<?php
class Upload extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('forum_model');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('language');
        $lang = $this->session->userdata('language');
        $this->lang->load('view', $lang);
        $this->config->set_item('language', $lang);
    }

    function index()
    {
        $this->session->userdata('logged_in');
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        $this->load->view('forum/header', $data);
        $this->load->view('upload_form', array('error' => ' ' ));
    }

    function do_upload()
    {
        $this->session->userdata('logged_in');
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        $config['upload_path'] = './images/user_image/';
        $config['allowed_types'] = 'jpg|png|gif';
        $config['max_size']	= '500';
        $config['max_width']  = '2048';
        $config['max_height']  = '1024';
        $config['overwrite']   = TRUE;
        $config['file_name'] = $session_data['id'];

        $this->load->library('upload', $config);


        if ( ! $this->upload->do_upload())
        {
            $this->session->userdata('logged_in');
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->view('forum/header', $data);
            $error = array('error' => $this->upload->display_errors());

            $this->load->view('upload_form', $error);
        }
        else
        {

            $this->session->userdata('logged_in');
            $session_data = $this->session->userdata('logged_in');
            $id = $session_data['id'];
            $config['image_library'] = 'gd2';
            $config['maintain_ratio'] = FALSE;
            $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
           // $config['source_image'] = './application/views/forum/images/user_image/'.$session_data['id'];
            $config['width'] = 128;
            $config['height'] = 128;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $data = $this->upload->file_name;
            $this->forum_model->update_img($id, $data);
            redirect('home/chamber', 'refresh');
            



        }
    }
}
