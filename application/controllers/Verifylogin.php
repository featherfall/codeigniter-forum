<?php
class VerifyLogin extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('user','',TRUE);
        $this->load->library('session');
        $this->load->helper('language');
        $lang = $this->session->userdata('language');
        $this->lang->load('view', $lang);
        $this->config->set_item('language', $lang);
    }

    function index()
    {
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('url');

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

        if($this->form_validation->run() == FALSE)
        {
            $this->load->view('forum/header_reg');
            $this->load->view('forum/auth_form');
        }
        else
        {
            redirect('home', 'refresh');
        }

    }

    function check_database($password)
    {

        $username = $this->input->post('username');

        $result = $this->user->login($username, md5($password));

        if($result)
        {
            foreach($result as $row)
            {
                $sess_array = array(
                    'id' => $row->id,
                    'username' => $row->login,
                    'level' => $row->level
                );
                $this->session->unset_userdata('guest');
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }
}