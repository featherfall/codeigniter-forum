<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//session_start();

class Home extends CI_Controller

{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('forum_model');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('language');
        $lang = $this->session->userdata('language');
        $this->lang->load('view_lang', $lang);
        if (!$this->session->userdata('logged_in')) {
            $sess_array = array(
                'id' => 0,
                'username' => 'guest',
                'level' => 0
            );
            $this->session->set_userdata('guest', $sess_array);
        }
    }

    public function index()
    {

        if ($this->session->userdata('logged_in')) {
            $data['categories'] = $this->forum_model->get_categories();
            $data['threads'] = $this->forum_model->get_last_threads();
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->view('forum/header', $data);
            $this->load->view('forum/index', $data);
            $this->load->view('forum/footer');


        } else {
            $data['categories'] = $this->forum_model->get_categories();
            $data['threads'] = $this->forum_model->get_last_threads();
            $session_data = $this->session->userdata('guest');
            $data['username'] = $session_data['username'];
            $this->load->view('forum/header_guest', $data);
            $this->load->view('forum/index', $data);
            $this->load->view('forum/footer');
        }
    }

    function logout()
    {
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect($this->input->server('HTTP_REFERER'));
    }

    public function show_category($id)
    {
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['category'] = $this->forum_model->get_current_category($id);
            $data['threads'] = $this->forum_model->get_threads($id);
            $this->load->view('forum/header', $data);
            $this->load->view('forum/view_category', $data);
            $this->load->view('forum/footer');

        } else {
            $session_data = $this->session->userdata('guest');
            $data['username'] = $session_data['username'];
            $data['category'] = $this->forum_model->get_current_category($id);
            $data['threads'] = $this->forum_model->get_threads($id);
            $this->load->view('forum/header_guest', $data);
            $this->load->view('forum/view_category', $data);
            $this->load->view('forum/footer');
        }

    }

    public function show_thread($id)
    {
        if ($this->session->userdata('logged_in')) {
            $this->load->helper('form');
            $this->load->library('form_validation');
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['posts'] = $this->forum_model->get_posts($id);
            $data['thread'] = $this->forum_model->get_current_thread($id);
            $this->load->view('forum/header', $data);
            $this->load->view('forum/view_thread', $data);
            $this->load->view('forum/footer');
        } else {
            $this->load->helper('form');
            $this->load->library('form_validation');
            $session_data = $this->session->userdata('guest');
            $data['username'] = $session_data['username'];
            $data['posts'] = $this->forum_model->get_posts($id);
            $data['thread'] = $this->forum_model->get_current_thread($id);
            $this->load->view('forum/header_guest', $data);
            $this->load->view('forum/view_thread', $data);
            $this->load->view('forum/footer');
        }
    }

    public function create_category()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        if ($session_data = $this->session->userdata('guest')) {
            $data['username'] = $session_data['username'];
            $this->load->view('forum/header_guest', $data);
            $this->load->view('forum/guest');
            $this->load->view('forum/footer');
        } else {
            if ($this->form_validation->run() === FALSE) {
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $this->load->view('forum/header', $data);
                $this->load->view('forum/create_category');
                $this->load->view('forum/footer');
            } else {
                $this->forum_model->create_category();
                redirect('home', 'refresh');
            }
        }
    }

    public function create_thread()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Subject', 'required');
        if ($session_data = $this->session->userdata('guest')) {
            $data['username'] = $session_data['username'];
            $this->load->view('forum/header_guest', $data);
            $this->load->view('forum/guest');
            $this->load->view('forum/footer');
        } else {
            if ($this->form_validation->run() === FALSE) {
                if ($session_data = $this->session->userdata('logged_in')) {
                    $data['username'] = $session_data['username'];
                    /*  $this->db->select('id');
                      $query = $this->db->get_where('users', array('login' => $user));
                      foreach ($query->result_array() as $row)
                      {
                         $id = $row['id'];
                      }
                    */
                    $data['categories'] = $this->forum_model->get_categories();
                    $this->load->view('forum/header', $data);
                    $this->load->view('forum/create_thread');
                    $this->load->view('forum/footer');
                } else {
                    redirect('Auth', 'refresh');
                }
            } else {
                $session_data = $this->session->userdata('logged_in');
                $id = $session_data['id'];
                $this->forum_model->create_thread($id);
                redirect('home', 'refresh');
            }
        }
    }

    public function reply($id)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('reply', 'Reply', 'required');
        if ($session_data = $this->session->userdata('guest')) {
            $data['username'] = $session_data['username'];
            $this->load->view('forum/header_guest', $data);
            $this->load->view('forum/guest');
            $this->load->view('forum/footer');
        } else {
            if ($this->form_validation->run() === FALSE) {
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $data['posts'] = $this->forum_model->get_posts($id);
                $data['thread'] = $this->forum_model->get_current_thread($id);
                $this->load->view('forum/header', $data);
                $this->load->view('forum/view_thread', $data);
                $this->load->view('forum/footer');
            } else {
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['id'];
                $data['thread'] = $this->forum_model->get_current_thread($id);
                $this->forum_model->submit_post($data);
                redirect('home/show_thread/' . $data['thread']['id'], 'refresh');


            }
        }
    }

    public function chamber()
    {
        if ($session_data = $this->session->userdata('guest')) {
            $data['username'] = $session_data['username'];
            $this->load->view('forum/header_guest', $data);
            $this->load->view('forum/guest');
            $this->load->view('forum/footer');
        } else {
            $this->session->userdata('logged_in');
            $this->load->helper('form');
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['id'] = $session_data['id'];
            $id = $data['id'];
            $data['user'] = $this->forum_model->get_user($id);
            $data['thread_stat'] = $this->forum_model->get_thread_stats($id);
            $data['post_stat'] = $this->forum_model->get_post_stats($id);
            $this->load->view('forum/header', $data);
            $this->load->view('forum/chamber', $data);
            $this->load->view('forum/footer');

        }
    }

    public function language($id)
    {
        if ($id == "1") {
            $language = "english";
            $this->session->set_userdata('language', $language);
            redirect($this->input->server('HTTP_REFERER'));
        }
        if ($id == "2") {
            $language = "russian";
            $this->session->set_userdata('language', $language);
            redirect($this->input->server('HTTP_REFERER'));
        }


    }

    public function edit_post($id)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('reply', 'Reply', 'required');
        $data['post'] = $this->forum_model->get_current_post($id);
        if ($this->session->userdata('logged_in')) {
            $level = $_SESSION['logged_in']['level'];
            if ($level === "1") {
                if ($this->form_validation->run() === FALSE) {
                    $session_data = $this->session->userdata('logged_in');
                    $data['username'] = $session_data['username'];
                    $data['post'] = $this->forum_model->get_current_post($id);
                    $this->load->view('forum/header', $data);
                    $this->load->view('forum/edit_post', $data);
                    $this->load->view('forum/footer');
                } else {
                    $user = $_SESSION['logged_in']['username'];
                    $session_data = $this->session->userdata('logged_in');
                    $data['username'] = $session_data['id'];
                    $this->forum_model->edit_post($id, $user);
                    redirect('home/show_thread/' . $data['post']['thread_id'], 'refresh');
                }
            }
            else
            {
                if ($_SESSION['logged_in']['id'] === $data['post']['author_id'])
                {
                    if ($this->form_validation->run() === FALSE) {
                        $session_data = $this->session->userdata('logged_in');
                        $data['username'] = $session_data['username'];
                        $data['post'] = $this->forum_model->get_current_post($id);
                        $this->load->view('forum/header', $data);
                        $this->load->view('forum/edit_post', $data);
                        $this->load->view('forum/footer');
                    } else {
                        $session_data = $this->session->userdata('logged_in');
                        $data['username'] = $session_data['id'];
                        $user = $_SESSION['logged_in']['username'];
                        $this->forum_model->edit_post($id, $user);
                        redirect('home/show_thread/' . $data['post']['thread_id'], 'refresh');
                    }
                }
                else {
                    $session_data = $this->session->userdata('logged_in');
                    $data['username'] = $session_data['username'];
                    $data['post'] = $this->forum_model->get_current_post($id);
                    $this->load->view('forum/header', $data);
                    $this->load->view('forum/edit_fail', $data);
                    $this->load->view('forum/footer');

                }


            }
        }

        else
        {
            $session_data = $this->session->userdata('guest');
            $data['username'] = $session_data['username'];
            $this->load->view('forum/header_guest', $data);
            $this->load->view('forum/guest');
            $this->load->view('forum/footer');
        }


    }
}
