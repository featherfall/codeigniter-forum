<div id="table" style="padding-bottom: 10px; padding-left: 15px">
<h2 style="text-align: center" xmlns="http://www.w3.org/1999/html"><?=lang('create_thread')?></h2>
    <hr style="width: 70%"/>
<?php echo validation_errors();?>
<?php echo form_open('home/create_thread') ?>
<?=lang('subject')?> <br>
<input type="input" name="name" /><br /><br/>
<?=lang('category')?>
    <br>
<select name="category_id">
    <?php foreach ($categories as $categories_item): ?>
    <option value="<?php echo $categories_item['id']?>" > <?php echo $categories_item['name']?></option>
    <?php endforeach ?>
</select>
<br><br>

<input class="button" type="submit" name="submit" value="<?=lang('create')?>" />


</form>
</div>