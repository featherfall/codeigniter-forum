<h3 style="text-align: center"><?=lang('welcome')?>, <?php echo $user['login']?>.</h3>
<hr/>
<hr/>
<div id="table" style="display: inline-block; padding-bottom: 20px; text-align: center">
<div> <?=lang('image')?> :</div>
    <br>
<img style="border: 1px solid #000000" src="<?=base_url()?>images/user_image/<?php echo $user['img']?>">
<br/><br/>

  <a href="<?=base_url()?>index.php/upload"><?=lang('change')?></a>
</div>
<div style="float: right; padding-right: 40px">
<div>
    <?=lang('y_email')?> <b><?php echo $user['email']?></b>
</div>
<div>
    <?=lang('y_registered')?> <b><?php echo $user['registered_at']?></b>
</div>
<?=lang('t_counter')?> : <b><?php echo $thread_stat;?></b> <br>
<?=lang('p_counter')?> : <b><?php echo $post_stat;?></b> <br>
</div>

