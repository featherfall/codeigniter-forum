<div id="table" style="display: inline-block">
<div style="float: left; border-right: 1px dashed gray; text-align: center"><h3><?=lang('category')?>:</h3>
<?php foreach ($categories as $category_item): ?>

    <div id="table" style="font-size: 15px; padding: 5px">
<a href="<?=base_url()?>index.php/admin/manage_content/<?=$category_item['id']?>"><?=$category_item['name']?></a> - <?=$category_item['description']?> <br>
    </div>
    <div id="table" style="padding: 10px; display: inline-block">
       <a style="color: darkred;" href="<?=base_url()?>index.php/admin/delete_category/<?=$category_item['id']?>"><?=lang('delete')?></a>
    </div>
    <div id="table" style="padding: 10px; display: inline-block;">
        <a href="<?=base_url()?>index.php/admin/edit_category/<?=$category_item['id']?>"> <?=lang('edit')?></a>
    </div>
    <br>
<?php endforeach ?>
</div>

<?php if ($id != FALSE): ?>
    <div style="float: left; padding-left: 20px; text-align: center"> <h3><?=lang('threads_in')?> "<?=$current_category['name']?>":</h3>
<?php foreach ($threads as $threads_item):?>
    <div style="font-size: 20px"><?=$threads_item['subject']; ?></div>
    <div style="padding: 5px; display: inline-block">
            <a style="color: darkred" href="<?=base_url()?>index.php/admin/delete_thread/<?=$threads_item['id']?>"><?=lang('delete')?></a>
        </div>
    <div style="padding: 5px; display: inline-block">
            <a href="<?=base_url()?>index.php/admin/edit_thread/<?=$threads_item['id']?>"> <?=lang('edit')?></a>
        </div>
        <br><br>
<?php endforeach; endif ?>
</div>
</div>