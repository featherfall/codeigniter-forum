
<table border="1" style="width: 100%; border-style: solid; border-spacing: 0px; color: gray;">
    <td style="width: 80%; border-style: solid; text-align: center"><?=lang('category')?></td>
    <td style="width: 20%; border-style: solid; text-align: center"> <?=lang('thread')?></td>

    <?php foreach ($categories as $categories_item): ?>
    <tr>

        <td id="table" style="width: 80%; border-style: solid; padding-top: 10px; padding-bottom: 10px;" >
            <img src="<?=base_url()?>images/feather.png" width="5%" style="float: left; padding-right: 5px">
            <h3><a href="<?=base_url()?>index.php/home/show_category/<?php echo $categories_item['id'];?>" class="hvr-underline-from-center">
                    <?php echo $categories_item['name'] ?>
                </a></h3>
                    <i style="color: gray"><?php echo $categories_item['description'] ?></i>
        </td>


        <td id="table" style="width: 20%; border-style: solid; text-align: center" >

            <?php
            $myThread = "";
            $myDate = "";
            $threadId = 0;
            foreach ($threads as $threads_item):
                if ($threads_item['category_id'] === $categories_item['id'])
                {
                    $myThread = $threads_item['subject'];
                    $myDate = $threads_item['date'];
                    $threadId = $threads_item['id'];
                    break;
                }
            endforeach;

            if(!$myThread) {
                $myThread = lang('no_threads');
            }
            ?>
            <?php if ($threadId){ ?>
            <a class="threads" href="<?=base_url()?>index.php/home/show_thread/<?=$threadId?>">
                <?=$myThread; ?>
            </a><br>
                <?=lang('created')?> <?=$myDate?>
            <?php } else echo $myThread;?>



        </td>


    </tr>
    <?php  endforeach ?>
</table>