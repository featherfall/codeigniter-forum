<h3><?=lang('changes')?>.</h3>
<div id="table" style="display: inline-block; width: 100%; padding: 0">
<?php foreach ($users as $user): ?>

    <div style="border: 1px solid #000000; margin-top: 50px; font-size: 20px">
        <?=lang('username')?>: <b><?php echo $user['login']?></b> <?=lang('email')?>: <b><?=$user['email']?></b> <?=lang('registered')?>: <?=$user['registered_at']?>
    </div>
    <a href="<?=base_url()?>index.php/admin/delete_user/<?=$user['id']?>"><?=lang('delete')?></a>
    <div style="border: dotted red; float: right; margin-top: 3px">
        <?=lang('status')?>: <?php if ($user['level']==="1")
        {
        ?> <a href="<?=base_url()?>index.php/admin/make_user/<?=$user['id']?>">Admin</a>
        <?php } else {?>
        <a href="<?=base_url()?>index.php/admin/make_admin/<?=$user['id'];?>">User</a> <?php } ?>
        <br></div>

<?php endforeach ?>
</div>
