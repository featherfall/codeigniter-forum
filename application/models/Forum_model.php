<?php
class Forum_model extends CI_Model
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->database();
        $this->load->library('session');
    }

    public function get_categories() {
        $query = $this->db->get('categories');
        return $query->result_array();
    }

    public function get_last_threads() {
        $sql = "select * from (select * from threads order by id desc) t group by category_id";
        //$this->db->select($this->db->select()->from('threads'));
       /* select * from
        (select * from threads order by id desc) t
group by category_id */
        //$this->db->order_by('id','asc');
        //$this->db->group_by('category_id');
        //$this->db->select_max('id','subject','date','category_id','author_id');
        //$this->db->order_by('date','desc');
        //$this->db->limit(1);
        //$query = $this->db->get('threads');
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function create_category()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
        );
        $this->db->insert('categories', $data);
    }

    public function create_thread($id)
    {
        $data = array(
            'subject' => $this->input->post('name'),
            'date' =>  $this->date = date("Y-m-d H:i:s"),
            'category_id' => $this->input->post('category_id'),
            'author_id' => $id
        );
        $this->db->insert('threads', $data);
    }

    public function get_current_category($id)
    {
        $query = $this->db->get_where('categories', array('id' => $id));
        return $query->row_array();
    }

    public function get_threads($id)
    {
        $this->db->select('users.id, users.login,threads.id,threads.subject,threads.date,threads.category_id,threads.author_id');
        $this->db->order_by('threads.id','desc');
        $this->db->join('users', 'users.id = threads.author_id','left');
        $query = $this->db->get_where('threads', array('category_id' => $id));

        return $query->result_array();
    }

    public function get_posts($id)
    {
        $this->db->select('users.id, users.login, users.email, users.img, posts.id as post_id, posts.content, posts.date, posts.thread_id, posts.author_id, threads.id, threads.subject, posts.edited_by, posts.edited_date');
        $this->db->order_by('posts.id','asc');
        $this->db->join('users', 'users.id = posts.author_id','left');
        $this->db->join('threads', 'threads.id = posts.thread_id','left');
        $query = $this->db->get_where('posts', array('thread_id' => $id));
        return $query->result_array();
    }

    public function get_current_thread($id)
    {
        $query = $this->db->get_where('threads', array('id' =>$id));
        return $query->row_array();
    }

    public function get_current_post($id){
        $query = $this->db->get_where('posts', array('id' =>$id));
        return $query->row_array();
    }

    public function submit_post($data)
    {
        $id = $data['thread']['id'];
        $id2 = $data ['username'];
        $data = array(
            'content' => $this->input->post('reply'),
            'date' => $this->date = date("Y-m-d H:i:s"),
            'thread_id' => $id,
            'author_id' => $id2
        );
        $this->db->insert('posts', $data);
    }

    public function get_user($id)
    {
        $query = $this->db->get_where('users', array('id'=>$id));
        return $query->row_array();
    }

    public function update_img($id, $data)
    {
        $data = array ('img' => $data);
        $this->db->where('id',$id);
        $this->db->update('users', $data);
    }

    public function get_thread_stats($id)
    {
        $this->db->where('author_id',$id);
        $num_threads = $this->db->count_all_results('threads');
        return $num_threads;


    }

    public function get_post_stats($id)
    {
        $this->db->where('author_id',$id);
        $num_posts = $this->db->count_all_results('posts');
        return $num_posts;


    }

    public function delete_category($id){
        $this->db->delete('categories', array('id' => $id));
    }

    public function delete_thread($id){
        $this->db->delete('threads', array('id' => $id));
    }

    public function delete_post($id){
        $this->db->delete('posts', array('id' => $id));
    }

    public function delete_user($id){
        $this->db->delete('users', array('id' => $id));
    }

    public function get_admin_content($id=FALSE){
        if ($id == FALSE){
            $query = $this->db->get('categories');
            return $query->result_array();
        }
        else {
            $query = $this->db->get_where('threads', array ('category_id' => $id));
            return $query->result_array();
        }
    }

    public function edit_category($id){

        $data = array(
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
        );
        $this->db->update('categories', $data, array('id' => $id));
    }

    public function edit_thread($id){
        $data = array(
            'subject' => $this->input->post('name'),
            'category_id' => $this->input->post('category_id')
        );
        $this->db->update('threads', $data, array('id' => $id));
    }

    public function edit_post($id, $user){
        $data = array(
            'content' => $this->input->post('reply'),
            'edited_by' => $user,
            'edited_date' => $this->date = date("Y-m-d H:i:s")
        );
        $this->db->update('posts', $data, array('id'=>$id));
    }

    public function get_users(){
        $query = $this->db->get('users');
        return $query->result_array();
    }

    public function make_admin($id){
        $value = "1";
        $data = array('level' => $value);
        $this->db->update('users', $data, array('id'=>$id));
    }

    public function make_user($id){
        $value = "0";
        $data = array('level' => $value);
        $this->db->update('users', $data, array('id'=>$id));
    }

}

//'date' =>  $this->date = date('l jS \of F Y h:i:s A'),