<?php
//header
$lang['home'] = 'Home';
$lang['create_thread'] = 'Create thread';
$lang['create_category'] = 'Create category';
$lang['admin_panel'] = 'Admin panel';
$lang['hello'] = 'Hello';
$lang['not_you'] = 'Not you';
$lang['log_out'] = 'Log out';
$lang['or'] = 'or';
$lang['back'] = 'Go Back';
//auth and reg
$lang['reg_success'] = 'You have successfully registered!';
$lang['username'] = 'Username';
$lang['password'] = 'Password';
$lang['email'] = 'Email Address';
$lang['pass_conf'] = 'Password Confirmation';
$lang['submit'] = 'Submit';
$lang['sign_in'] = 'Sign in';
$lang['register'] = 'Register';
// index
$lang['category'] = "Category";
$lang['thread'] = "Last Thread";
$lang['all_threads'] = "Threads";
$lang['created'] = 'created';
$lang['no_threads'] = 'No threads';

//create
$lang['create_category'] = 'Create new category';
$lang['create_thread'] = 'Create new thread';
$lang['title'] = 'Title';
$lang['description'] = 'Description';
$lang['create'] = 'Create';
$lang['subject'] = 'Subject';
//view
$lang['started'] = 'Author';
$lang['at'] = 'at';
$lang['posted'] = 'posted this at';
$lang['reply'] = 'Reply';
$lang['edit_post_fail'] = 'You can only edit your own posts!';
$lang['last_edited'] = 'Last time was edited by';

//manage
$lang['manage_cont'] = 'Manage content';
$lang['manage_users'] = 'Manage users';
$lang['delete'] = 'delete';
$lang['edit'] = 'edit';
$lang['threads_in'] = 'Threads in category';
$lang['status'] = 'Account status';
$lang['registered'] = 'Registered at';
$lang['changes'] = 'Account status changes will work next log-in';
//chamber
$lang['welcome'] = 'Welcome to your Chamber';
$lang['image'] = 'Profile image';
$lang['change'] = 'Change';
$lang['y_email'] = 'Your email is';
$lang['y_registered'] = 'You was registered at';
$lang['t_counter'] = 'Thread counter';
$lang['p_counter'] = 'Post counter';
$lang['posts'] = 'Posts';
//upload
$lang['upload_text'] = 'JPG, PNG, GIF allowed, max size : 500kb. Image will be auto re-sized to 128x128';
$lang['browse'] = 'Browse..';
$lang['upload'] = 'Upload';
//admin
$lang['welcome_admin'] = 'Welcome to Admin Panel';
$lang['admin_fail'] = 'Sorry, this is restricted area. Contact administration for more info.';
$lang['guest'] = 'Sorry, for registered users only!';
$lang['date'] = 'Date';


