<?php
//header
$lang['home'] = 'Домой';
$lang['create_thread'] = 'Создать тред';
$lang['create_category'] = 'Создать тред';
$lang['admin_panel'] = 'Панель администратора';
$lang['hello'] = 'Привет';
$lang['not_you'] = 'Не вы';
$lang['log_out'] = 'Выйти';
$lang['or'] = 'или';
$lang['back'] = 'Вернуться назад';
//auth and reg
$lang['reg_success'] = 'Вы успешно зарегестрировались!';
$lang['username'] = 'Имя пользователя';
$lang['password'] = 'Пароль';
$lang['email'] = 'Email адрес';
$lang['pass_conf'] = 'Подтвердите пароль';
$lang['submit'] = 'Отправить';
$lang['sign_in'] = 'Войти';
$lang['register'] = 'Зарегестрироваться';
// index
$lang['category'] = "Категория";
$lang['thread'] = "Последний тред";
$lang['all_threads'] = "Треды";
$lang['created'] = 'создан';
$lang['posts'] = 'Постов';
$lang['no_threads'] = 'Нет тредов';
//create
$lang['create_category'] = 'Создать новую категорию';
$lang['create_thread'] = 'Создать новый тред';
$lang['title'] = 'Заголовок';
$lang['description'] = 'Описание';
$lang['create'] = 'Создать';
$lang['subject'] = 'Тема';
//view
$lang['started'] = 'Автор';
$lang['at'] = 'в';
$lang['posted'] = 'отправлено';
$lang['reply'] = 'Ответить';
$lang['edit_post_fail'] = 'Вы можете редактировать только свои собственные посты!';
$lang['last_edited'] = 'Последний раз редактировалось';
//manage
$lang['manage_cont'] = 'Редактировать контент';
$lang['manage_users'] = 'Редактировать пользователей';
$lang['delete'] = 'удалить';
$lang['edit'] = 'править';
$lang['threads_in'] = 'Треды в категории';
$lang['status'] = 'Статус аккаунта';
$lang['registered'] = 'Зарегестрирован';
$lang['changes'] = 'Изменения статуса аккаунта будут применены при следующем входе';
//chamber
$lang['welcome'] = 'Добро Пожаловать в Убежище';
$lang['image'] = 'Изображение профиля';
$lang['change'] = 'Изменить';
$lang['y_email'] = 'Ваш email';
$lang['y_registered'] = 'Вы зерегестрировались';
$lang['t_counter'] = 'Счетчик тредов';
$lang['p_counter'] = 'Счетчик постов';
//upload
$lang['upload_text'] = 'JPG, PNG, GIF разрешены, максимальный размер : 500kb. Изображение будет автоматически сжато до 128x128';
$lang['browse'] = 'Найти..';
$lang['upload'] = 'Загрузить';
//admin
$lang['welcome_admin'] = 'Добро Пожаловать в Панель Администратора';
$lang['admin_fail'] = 'Извините, это закрытая зона. Обратитесь к администрации для получения информации.';
$lang['guest'] = 'Извините, только для зарегестрированных пользователей!';
$lang['date'] = 'Дата';
